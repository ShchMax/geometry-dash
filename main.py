import pygame
from pygame.locals import *
from math import *
import time

c = None
w = 800
h = 600
FPS = 60
vx = 10
dead = False
win = False
clock = None
p = None
gc = None
speed = 0
t1me = 0
gt = 0
grounds = []

def clear():
	pygame.draw.rect(c, (0, 255, 0), (0, 0, w, h))

def sign(a):
	if (a < 0):
		return -1
	return 1

class Person():
	surf = None
	iw = 50
	ih = 50
	x = 0
	y = h
	v = 0
	g = 4
	angle = 0
	dv = 35
	rotcnt = 4
	dx = 0
	dy = h
	yl = 0
	st = 0
	grd = 0
	def __init__(self):
		self.surf = pygame.transform.scale(pygame.image.load("images/person.png").convert_alpha(), (self.iw, self.ih))
	def die(self):
		global dead
		dead = True
	def move(self):
		self.yl = self.y
		if (self.v != 0 or self.y != self.dy):
			self.y -= self.v
			self.y = min(self.dy, self.y)
			self.v -= self.g
			self.angle = (self.angle - self.rotcnt * 360 / (8 * self.dv / self.g)) % 360
		if (self.y == self.dy):
			self.v = 0
			self.angle = 0
		self.paint()
	def paint(self):
		surf = pygame.transform.rotate(self.surf, self.angle)
		rect = surf.get_rect(center=(self.x + self.iw // 2, self.y - self.ih // 2))
		c.blit(surf, rect)
		pygame.display.update()
	def jump(self):
		if (self.y != self.dy):
			return
		self.v = self.dv
	def ground(self, i):
		x = self.x
		y = self.y
		yl = self.yl
		if (i.x - t1me * speed <= self.x + self.iw and i.x - t1me * speed + self.iw >= x):
			pass
		else:
			if (i.i == self.grd):
				self.grd = 0
				self.dy = h
			return
		if (yl <= i.h and y >= i.h):
			self.dy = i.h
			self.y = i.h
			self.grd = i.i
		elif (yl > i.h and y <= i.h or (y - self.ih) <= i.h <= y):
			self.die()

class Ground():
	h = 0
	x = 0
	i = 0
	vis = False
	def __init__(self, hi, x, v):
		global gt
		gt += 1
		self.i = gt
		self.h = h - hi * p.ih
		self.x = x * p.iw
		self.vis = v
	def paint(self):
		if (not self.vis):
			return
		pygame.draw.line(c, gc, [self.x - t1me * speed, self.h], [self.x - t1me * speed + p.iw, self.h], 3)
		pygame.display.update()

class Block():
	upper = None
	downer = None
	middle = None
	x = 0
	y = 0
	def __init__(self, x, y):
		self.x = x * p.iw
		self.y = h - y * p.ih
		upper = Ground(y + 1, x, 1)
		downer = Ground(y, x, 1)
		middle = Ground(y + 0.5, x, 0)
		grounds.append(upper)
		grounds.append(downer)
		grounds.append(middle)
	def paint(self):
		pygame.draw.rect(c, (0, 0, 0), (self.x - t1me * speed, self.y - p.ih, p.iw, p.ih))
		pygame.draw.line(c, gc, [self.x - t1me * speed, self.y], [self.x - t1me * speed, self.y - p.ih], 3)
		pygame.draw.line(c, gc, [self.x - t1me * speed + p.iw, self.y], [self.x - t1me * speed + p.iw, self.y - p.ih], 3)
		pygame.display.update()

a = input("Enter filename:\n")

pygame.init()
c = pygame.display.set_mode((w, h))
clock = pygame.time.Clock()
p = Person()
cnt = 0
count = 20
v = 10
gc = (255, 255, 255)
speed = 700
grounds = []
blocks = []
last = 0

f = open("./maps/" + a + ".gdm", "r")
cnting = 0
for i in f.readlines():
	cnting += 1
	try:
		a, b = map(int, i.split())
		last = max(last, a)
	except:
		raise ValueError("Incorrect map, {} line.".format(cnting))
	blocks.append(Block(a, b))

last += 2

def paint():
	p.move()
	for i in blocks:
		i.paint()
	for i in grounds:
		p.ground(i)
		i.paint()

def explose():
	global cnt
	cnt += 1
	paint()
	pygame.draw.circle(c, (255, int(255 * (cnt / count)), 0), (int(p.x + p.iw // 2), int(p.y - p.ih // 2)), int(max(p.ih, p.iw) * (cnt / count)))
	pygame.display.update()

y0 = p.y

while p.x <= w:
	clear()
	if (dead):
		explose()
		if (cnt == count):
			cnt = 0
			dead = False
			p = Person()
			time.sleep(1)
			t1me = 0
	if (last * p.iw < t1me * speed):
		p.angle = (p.angle - p.rotcnt * 360 / (8 * p.dv / p.g)) % 360
		p.x += 10
		p.y = y0 - int(sqrt(p.x) + sqrt(p.y))
		p.paint()
	events = pygame.event.get()
	for e in events:
		if (e.type == QUIT):
			pygame.quit()
			raise SystemExit()
	if (not dead):
		keys = pygame.key.get_pressed()
		if (keys[K_SPACE]):
			p.jump()
		paint()
	y0 = p.y
	if (not dead):
		t1me += 1 / FPS
	clock.tick(FPS)
